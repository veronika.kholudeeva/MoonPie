"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from cakes import views
from django.conf.urls import include

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'about/', views.about, name = 'about'),
    re_path(r'catalog/', views.catalog, name = 'catalog'),
    #path('catalog/', views.catalog, name = 'catalog'),
    re_path(r'cabinet/', views.cabinet, name = 'cabinet'),
    path('reg/', views.reg, name = 'reg'),
    path('red/', views.red, name = 'red'),
    path('log/', views.log, name = 'log'),
    path('cart/', views.cart, name='cart'),
    path('kor/', views.kor, name='kor'),
    path('zakaz/', views.zakaz, name='zakaz'),
    path('logout/', views.logout_v, name = 'logout_v'),
    re_path(r'korzina/', views.korzina, name = 'kor2'),
    path('admin/', admin.site.urls),
]
