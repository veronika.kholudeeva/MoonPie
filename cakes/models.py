from django.db import models


class Customer(models.Model):
    id_customer = models.AutoField('код покупателя', primary_key=True)
    name = models.CharField('имя покупателя', max_length=50, blank=False)
    surname = models.CharField('фамилия покупателя', max_length=50, blank=False)
    middle_name = models.CharField('отчество покупателя', max_length=50)
    sex = models.CharField('пол покупателя', max_length=50, blank=False)
    date_of_birth = models.DateField('дата рождения', blank=False)
    email = models.EmailField('e-mail покупателя', blank=False)
    phone = models.CharField('номер телефона покупателя', max_length=20, blank=False)
    login = models.CharField('логин',max_length=50, unique=True, blank=False)
    password = models.CharField('пароль', max_length=200, blank=False)

    cart = models.ManyToManyField('Cart')

    class Meta:
        verbose_name = 'Покупатель'
        verbose_name_plural = 'Покупатели'

class Product(models.Model):
    id_product = models.AutoField('код товара',primary_key=True)
    name_product = models.CharField('название товара', max_length=100)
    price = models.IntegerField('цена')
    type_product = models.IntegerField('тип товара')
    taste = models.IntegerField('вкус')
    layers = models.IntegerField('коржи')
    filling = models.IntegerField('начинка')
    tiers = models.IntegerField('ярусы')
    photo = models.CharField('название файла с фото', max_length=100, blank=False)

    cart_product = models.ManyToManyField('Cart')

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

class Cart(models.Model):
    id_cart = models.AutoField('код строки корзины',primary_key=True)
    login = models.ForeignKey(Customer, related_name="покупатель", on_delete = models.CASCADE, null=True)
    id_product = models.ForeignKey(Product, related_name="товар", on_delete = models.CASCADE)
    login_anonymus=models.CharField('логин не авторизованных пользователей', max_length=20, default='Net')
    quantity = models.IntegerField('кол-во', default=1)
    photo = models.ForeignKey(Product, related_name="фотография", on_delete = models.CASCADE)
    name_product = models.ForeignKey(Product, related_name="название", on_delete = models.CASCADE)
    price_summary = models.IntegerField("цена_товара")
    
    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзина'

class Order(models.Model):
    id_order = models.AutoField('код заказа',primary_key=True)
    amount = models.IntegerField('общее количество товара', blank=False)
    amount_of_money = models.IntegerField('общая стоимость')
    order_date = models.DateField('дата заказа')
    date_of_payment = models.DateField('дата оплаты')
    note = models.TextField('комментарий к заказу')
    status = models.CharField('статус заказа', max_length=50, default="Обрабатывается")
    id_customer = models.ForeignKey(Customer, on_delete = models.CASCADE)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

class Order_line(models.Model):
    id_order_line = models.AutoField('код строки корзины',primary_key=True)
    id_order  = models.ForeignKey(Order, related_name="код_заказа_строки", on_delete = models.CASCADE)
    id_product = models.ForeignKey(Product, related_name="товар_строки", on_delete = models.CASCADE)
    quantity_line = models.IntegerField("количество_товара_для_заказа")
    price_summary_line = models.IntegerField("стоимость_товара_для_заказа")
    class Meta:
        verbose_name = 'Строка заказа'
        verbose_name_plural='Строки заказа'
